FROM node:10.15.0-jessie-slim

# workdir (also creates if not exists)
WORKDIR /opt/app
RUN chown -R node:node /opt/app

# copy code + npm auth (from bus-ci)
COPY --chown=node:node . /opt/app/

# use node user
USER node

# Install all (dev-deps needed for tsc). Fails if the lockfile is not up-to-date
RUN yarn install --no-progress --frozen-lockfile

# compile tsc
RUN yarn run tsc

# prune dev-dependencies (included audit)
RUN npm prune --production

ENV NODE_ENV=production

EXPOSE 8080

CMD [ "node", "dist/main.js" ]
