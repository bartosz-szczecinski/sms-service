import axios from 'axios';
import * as qs from 'qs';
import { SmsPostRequest, SmsPostResponse } from './models';
import { ApiGatewayMetadata, mapException } from '@bus/common';
import { Injectable, Inject } from '@nestjs/common';

@Injectable()
export class SmsService {
    constructor(
        @Inject('SmsServiceUrl') private readonly remoteUrl: string,
        @Inject('ApiKey') private readonly apiKey?: string,
    ) {
    }

    /**
     * Creates a SMS.
     * @param body Body object for the for the operation. See detailed docs in the arguments type.
     * @param metadata API gateway metadata object for the for the operation. It will help to pass
     * through request and consumer ids for deeply keeping track of the execution chain.
     * @throws {BadRequestException} In case of validation errors
     * @throws {NotAuthorizedException} In case of API gateway authorization error
     * @throws {ForbiddenException} In case that the consumer is not allowed to perform the action
     * @throws {BusInternalServerErrorException} In case that an internal server error occurred
     * and the consumer should consult the #business-services-support channel.
     */
    public async createSMS(
        body: SmsPostRequest,
        metadata: ApiGatewayMetadata,
    ): Promise<SmsPostResponse> {
        const requestPath = `send`;
        return axios({
            method: 'post',
            baseURL: this.remoteUrl,
            headers: {
                'x-api-key': this.apiKey || '',
                'x-consumer-id': metadata.consumerId || '',
                'x-request-id': metadata.requestId || '',
            },
            maxContentLength: 50000000,
            url: requestPath,
            data: body,
        }).then(response => {
            return response.data;
        }).catch(error => {
            if (error.response) {
                throw mapException(error.response.data);
            } else {
                throw mapException(error);
            }
        });
    }
}
