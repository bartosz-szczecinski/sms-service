/**
 * Types of SMS supported by the provider. Might impact delivery delay and pricing.
 */
export enum SmsType {
    /**
     * The SMS is to be market as a promotional / marketing message and will be delivered with lower
     * priority / cost
     */
    Promotional = 'Promotional',
    /**
     * The SMS is used to verify a transaction (e.g. containing a TAN or PIN) and should be delivered with
     * highest priority. This option incures higher costs.
     */
    Transactional = 'Transactional'
}
