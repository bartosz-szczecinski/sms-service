import * as fs from 'async-file';
import { MiddlewareConsumer, Module, NestModule, MiddlewareFunction, NestMiddleware, Injectable, Inject } from '@nestjs/common';
import { AppController, INJECT_SERVICE_PROPS, INJECT_SERVICE_MANUAL, INJECT_SERVICE_SWAGGER } from './app.controller';
import { WinstonModule } from 'nest-winston';
import { Logger } from 'winston';
import winston = require('winston');
import { SmsModule } from './sms/sms.module';

const servicePropsLoader = async () => fs.readFile('./service.json')
    .then(fileStr => JSON.parse(fileStr))
    .catch(() => { throw new Error('There is no ./service.json in your project folder. run `$ bus init api`'); });
const serviceManualLoader = async () => fs.readFile('./SERVICE.md')
    .then((buffer: Buffer) => buffer.toString())
    .catch(() => { throw new Error('There is no ./SERVICE.md in your project folder.'); });
const serviceSwaggerLoader = async () => fs.readFile('./service.swagger.json')
    .then(fileStr => JSON.parse(fileStr))
    .catch(() => { throw new Error('There is no ./service.swagger.json in your project folder. run `$ bus generate swagger`'); });

@Injectable()
export class BUSLogMiddleware implements NestMiddleware {
    constructor(
        @Inject('winston')
        private readonly logger: Logger
    ) { }

    public resolve(): MiddlewareFunction {
        return (req, res, next) => {
            if (req.headers['user-agent'] && !req.headers['user-agent'].includes('ELB-HealthChecker')) { // log only non ELB health checks ("user-agent":"ELB-HealthChecker/2.0")
                this.logger.info('NestMiddleware request', {
                    request: {
                        method: req.method,
                        baseUrl: req.baseUrl,
                        ip: req.ip,
                        headers: req.headers,
                        body: req.body,
                    }
                });
            }
            next!();
        };
    }
}

@Module({
    imports: [
        SmsModule,
        BUSLogMiddleware,
        WinstonModule.forRoot({
            level: 'info',
            format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
            transports: [
                new winston.transports.Console(),
            ]
        }),
    ],
    providers: [
        { provide: INJECT_SERVICE_PROPS, useFactory: servicePropsLoader },
        { provide: INJECT_SERVICE_MANUAL, useFactory: serviceManualLoader },
        { provide: INJECT_SERVICE_SWAGGER, useFactory: serviceSwaggerLoader },
    ],
    controllers: [AppController],
})
export class AppModule implements NestModule {
    public configure(consumer: MiddlewareConsumer): void {
        consumer
            .apply(BUSLogMiddleware)
            .forRoutes('*');
    }
}
