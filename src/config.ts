import { ApiConfig } from "@bus/common";

import { SmsConfig } from "./sms/sms.config";

interface StageConfig {
  swaggerHost: string;
}

const STAGE_CONFIG: Record<string, StageConfig> = {
  local: {
    swaggerHost: "api.dev.services.vwfs.io"
  },
  dev: {
    swaggerHost: "api.dev.services.vwfs.io"
  },
  int: {
    swaggerHost: "api.int.services.vwfs.io"
  },
  cons: {
    swaggerHost: "api.cons.services.vwfs.io"
  },
  prod: {
    swaggerHost: "api.services.vwfs.io"
  }
};

let stageConfig: StageConfig;

if (process.env.STAGE) {
  stageConfig = STAGE_CONFIG[process.env.STAGE];
} else {
  stageConfig = STAGE_CONFIG.local;
}

export const smsConfig = (): SmsConfig => ({
  region: process.env.SNS_REGION || "eu-west-1",
  stage: process.env.STAGE || "local"
});

export const appConfig = {
  port: process.env.SERVICE_PORT || 8080,
  requestLimit: process.env.REQUEST_LIMIT || "50mb",
  basePath: process.env.SERVICE_BASE_PATH || "/sms-service",
  stage: process.env.STAGE
};

export const apiConfig: ApiConfig = {
  swaggerTitle: "VW FS SMS Service",
  swaggerDescription:
    "Generic SMS Service used to deliver messages to end users.",
  swaggerVersion: "1.0",
  swaggerSchemes: ["https"],
  swaggerHost: stageConfig.swaggerHost,
  swaggerBasePath: appConfig.basePath
};
