import { Expose, Exclude } from 'class-transformer';
import { IsOptional, IsEnum, IsString, IsNotEmpty } from 'class-validator';
import { SmsType } from '../../models/enums';

@Exclude()
export class SmsPostRequest {
    /**
     * Type of the message. This impacts delivery priority and cost. Promotional will be used by default.
     */
    @Expose()
    @IsOptional()
    @IsEnum(SmsType)
    public type?: SmsType;
    /**
     * The name from which the message will appear to be sent. 11 characters maximum, at least one letter.
     */
    @Expose()
    @IsOptional()
    @IsString()
    public senderId?: string;
    /**
     * The content of the message to be sent. If the message exceeds the SMS length limit it will be
     * automatically split into multiple messages.
     */
    @Expose()
    @IsNotEmpty()
    @IsString()
    public message: string;
    /**
     * The number to which the message is to be delivered to. E.164 format.
     */
    @Expose()
    @IsNotEmpty()
    @IsString()
    public receiverNumber: string;
}
