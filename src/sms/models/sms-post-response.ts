import { Expose, Exclude } from 'class-transformer';
import { IsNotEmpty, IsUUID, IsOptional } from 'class-validator';

@Exclude()
export class SmsPostResponse {
    /**
     * Amazon SNS Request Id
     */
    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public requestId: string;
    /**
     * Amazon SNS Message Id - can be used to look up the message delivery details in Cloud Logs
     */
    @Expose()
    @IsOptional()
    @IsUUID()
    public messageId?: string;
}
