import { SmsService } from './exports';
import { ValidationPipe, ApiGatewayMetadata, Metadata, BusApiOperation, ConsumerIdGuard } from '@bus/common';
import { SmsPostRequest, SmsPostResponse } from './models';
import { Body, Controller, UseGuards, Inject } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Logger } from 'winston';

@Controller()
@UseGuards(ConsumerIdGuard)
@ApiBearerAuth()
@ApiUseTags('Sms')
export class SmsController {
    constructor(
        private smsService: SmsService,
        @Inject('winston') private logger: Logger,
    ) {
    }

    /**
     * Creates a SMS.
     */
    @BusApiOperation({
        post: '/send',
        title: 'Create SMS',
        description: 'Creates a SMS.',
        responseType: SmsPostResponse,
        responseDescription: 'Returns the metadata related to the create request.',
        responseStatus: 201,
    })
    public async createSMS(
        @Metadata() metadata: ApiGatewayMetadata,
        @Body(new ValidationPipe(false)) body: SmsPostRequest,
    ): Promise<SmsPostResponse> {
        this.logger.info('FLOW STEP 1 SmsService.create START.', metadata);
        return this.smsService.create(body);
    }
}
