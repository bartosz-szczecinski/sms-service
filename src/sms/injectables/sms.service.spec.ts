import { Test } from "@nestjs/testing";
import { WinstonModule } from "nest-winston";

import winston = require("winston");
import { SmsService } from "./sms.service";

jest.mock("aws-sdk");

import { SNS } from "aws-sdk";

interface ServiceError {
  response: {
    error: string;
    statusCode: number;
    message?: string;
  };
  status: number;
  message?: any;
}

describe("SMS Service", () => {
  let service: SmsService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [
        WinstonModule.forRoot({
          level: "info",
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json()
          ),
          transports: [new winston.transports.Console()]
        })
      ],
      providers: [
        SmsService,
        { provide: "Config", useValue: { region: "eu-central-1" } }
      ]
    }).compile();

    service = module.get<SmsService>(SmsService);
  });

  describe("create", () => {
    test("Creates the SMS if message and receipent number given", async () => {
      const result = await service.create({
        message: "test",
        receiverNumber: "+48123456789"
      });

      expect(result.messageId).toBeDefined();
      expect(result.requestId).toBeDefined();
    });

    test("Requires a valid phone number to be provided", async () => {
      return service
        .create({
          message: "test",
          receiverNumber: "48123456789"
        })
        .catch((e: ServiceError) => {
          expect(e.response.statusCode).toEqual(400);
        });
    });

    test("Fails gracefully if internal error occurred", async () => {
      jest.spyOn(SNS.prototype, "publish").mockImplementation(() => {
        throw new Error("Test error");
      });

      return service
        .create({
          message: "test",
          receiverNumber: "+48123456789"
        })
        .catch((e: ServiceError) => {
          expect(e.response.statusCode).toEqual(500);
          SNS.prototype.publish["mockRestore"]();
        });
    });

    describe("SenderId support", () => {
      test("Attempts to set the SenderId if provided", async () => {
        jest.clearAllMocks();
        let spy = jest.spyOn(SNS.prototype, "setSMSAttributes");
        const result = await service.create({
          message: "test",
          receiverNumber: "+48123456789",
          senderId: "testsender"
        });

        expect(spy).toHaveBeenCalled();

        expect(result.messageId).toBeDefined();
        expect(result.requestId).toBeDefined();

        SNS.prototype.setSMSAttributes["mockRestore"]();
      });

      test("Fails gracefully if invalid sender id provided", async () => {
        return service
          .create({
            message: "test",
            receiverNumber: "+48123456789",
            senderId: "test sender"
          })
          .catch((e: ServiceError) => {
            expect(e.response.statusCode).toEqual(400);
          });
      });

      test("Fails gracefully if internal error occurred", async () => {
        const mockError = "test";
        jest.spyOn(SNS.prototype, "setSMSAttributes").mockImplementation(() => {
          throw new Error(mockError);
        });

        return service
          .create({
            message: "test",
            receiverNumber: "+48123456789",
            senderId: "testsender"
          })
          .catch((e: ServiceError) => {
            expect(e.response.statusCode).toEqual(500);
            SNS.prototype.setSMSAttributes["mockRestore"]();
          });
      });
    });
  });
});
