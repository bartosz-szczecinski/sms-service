export const isE164 = (number: string): boolean => {
  return !!number.match(/^\+[1-9]\d{1,14}$/);
};

export const isValidSenderId = (senderId: string): boolean => {
  // longer than 11 characters
  if (senderId.length > 11) return false;

  // does not contain a letter
  if (!senderId.match(/[a-zA-Z]/)) return false;

  // contains non-alphanumeric characters
  if (senderId.match(/[^a-zA-Z0-9]/)) return false;
  return true;
};
