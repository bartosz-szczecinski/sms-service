import { isE164, isValidSenderId } from "./validate";

describe("SMS Service Validations", () => {
  test("E.164 phone number validation", () => {
    // missing +
    expect(isE164("48509035988")).toBe(false);

    // zero as 2nd digit
    expect(isE164("+048509035988")).toBe(false);

    // number is too long
    expect(isE164("+4812345678901234567")).toBe(false);

    expect(isE164("+48123456789")).toBe(true);
  });

  test("SenderId validation", () => {
    expect(isValidSenderId("VWFS")).toBe(true);

    // no space
    expect(isValidSenderId("VW FS")).toBe(false);

    // at least one letter
    expect(isValidSenderId("49123456789")).toBe(false);

    // at max 11 characters
    expect(isValidSenderId("VolkswagenFinancialServices")).toBe(false);
  });
});
