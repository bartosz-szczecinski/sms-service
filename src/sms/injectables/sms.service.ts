import { Inject, Injectable } from "@nestjs/common";
import { mapException } from "@bus/common";
import { SmsPostRequest, SmsPostResponse } from "../models";
import { Logger } from "winston";

import { SmsConfig } from "../sms.config";
import { SNS } from "aws-sdk";

import { isE164, isValidSenderId } from "./validate";

interface ErrorObject {
  message?: any;
  error?: string;
  statusCode: number;
}

interface ComputedErrorObject extends ErrorObject {
  response: {
    data: ErrorObject;
  };
}

function pickError(error: ComputedErrorObject) {
  if (error.response) {
    throw mapException(error.response.data);
  }
  throw mapException(error);
}

@Injectable()
export class SmsService {
  /**
   * Default constructor injecting all components that are required for object storage.
   * @param config Configuration for S3 bucket name and region
   * @param logger Logger component responsible for all logging tasks
   */
  constructor(
    @Inject("Config")
    private config: SmsConfig,
    @Inject("winston")
    private readonly logger: Logger
  ) {}

  public async create(body: SmsPostRequest): Promise<SmsPostResponse> {
    if (!isE164(body.receiverNumber)) {
      throw mapException({
        message:
          "Please provide a valid, E.164 compliant receiver phone number.",
        statusCode: 400
      });
    }

    const params: SNS.PublishInput = {
      Message: body.message,
      PhoneNumber: body.receiverNumber
    };

    const sns = new SNS({
      region: this.config.region
    });

    if (body.senderId) {
      if (isValidSenderId(body.senderId)) {
        this.logger.info(
          `[sms-service] Attempting to set SenderID to ${body.senderId}`
        );
        try {
          await sns
            .setSMSAttributes({
              attributes: {
                DefaultSenderID: body.senderId
              }
            })
            .promise();
        } catch (error) {
          this.logger.error(error);
          throw pickError(error);
        }
      } else {
        throw mapException({
          message:
            "Please provide a valid SenderId (max 11, alphanumeric characters, one letter required).",
          statusCode: 400
        });
      }
    }

    try {
      const response = await sns.publish(params).promise();

      this.logger.info("[sms-service] Message published");

      return {
        requestId: response.$response.requestId,
        messageId: response.MessageId
      };
    } catch (error) {
      this.logger.error(error);
      throw pickError(error);
    }
  }
}
