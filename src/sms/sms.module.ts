import { SmsService, ValueGenerator } from './exports';
import { SmsController } from './sms.controller';
import { smsConfig } from './exports';
import { Module } from '@nestjs/common';

@Module({
    providers: [
        SmsService,
        ValueGenerator,
        { provide: 'Config', useFactory: smsConfig },
    ],
    controllers: [
        SmsController,
    ],
})
export class SmsModule {
}
