import { Controller, Get, Header, Inject } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';

export const INJECT_SERVICE_PROPS = 'INJECT_SERVICE_PROPS';
export const INJECT_SERVICE_MANUAL = 'INJECT_SERVICE_MANUAL';
export const INJECT_SERVICE_SWAGGER = 'INJECT_SERVICE_SWAGGER';

@Controller()
@ApiBearerAuth()
export class AppController {

    constructor(
        @Inject(INJECT_SERVICE_PROPS) private readonly props: any,
        @Inject(INJECT_SERVICE_MANUAL) private readonly manual: string,
        @Inject(INJECT_SERVICE_SWAGGER) private readonly swagger: any,
    ) { }

    @Get('health')
    public get(): string {
        return 'Healthy!';
    }

    @Get('portal/api')
    public getApi(): any {
        return this.swagger;
    }

    /**
     * Returns a markdown description of the parent application. Note that the application needs to provide
     * a SERVICE.md in the execution root (e.g. /dist) folder of the service container.
     */
    @Header('Content-Type', 'text/plain')
    @Get('portal/manual')
    public getManual(): string {
        return this.manual;
    }

    /**
     * Returns a config json with basic metadata of the parent application. Note that the application
     * needs to provide a portal.json in the execution root (e.g. /dist) folder of the service container.
     * the service.json requires the following format:
     */
    @Get('portal/config')
    public getConfig(): any {
        return this.props;
    }
}
