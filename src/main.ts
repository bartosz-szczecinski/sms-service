import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import { AppModule } from './app.module';
import { appConfig } from './config';

/**
 * Bootstraps the Nest-App.
 */
async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.setGlobalPrefix(appConfig.basePath);
    app.use(express.json({ limit: appConfig.requestLimit }));
    await app.listen(appConfig.port);
}
bootstrap();
