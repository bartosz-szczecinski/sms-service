import { ModelFactory, ModelDescriptions } from '@bus/models';
import * as mergedModel from './merged.lmodels.json';

const smsModel = mergedModel as any as ModelDescriptions;
export const SmsModelFactory = ModelFactory(smsModel);
