export class SNS {
  setSMSAttributes() {
    return {
      promise: () => Promise.resolve(this)
    };
  }

  publish() {
    return {
      promise: () =>
        Promise.resolve({
          $response: {
            requestId: "1234"
          },
          MessageId: "0987"
        })
    };
  }
}
