import { HttpStatus } from "@nestjs/common";
import { config } from "dotenv";
import * as request from "supertest";

config({ path: `.env` });
const endpointURL =
  process.env.STAGE === "local"
    ? "http://localhost:8080"
    : `https://api.${process.env.STAGE}.services.vwfs.io`;

const headers = {
  "content-type": "application/json",
  accept: "application/json",
  "x-api-key": process.env.API_KEY,
  ...(process.env.STAGE === "local" ? { "x-consumer-id": "1234" } : {})
};

describe("/sms/send endpoint", () => {
  const smsEdnpoint = "/sms-service/send";

  test("Should return Bad Request Error when payload is missing expected data", async () => {
    await request(endpointURL)
      .post(smsEdnpoint)
      .set(headers)
      .send({})
      .expect(HttpStatus.BAD_REQUEST);
  });
});
