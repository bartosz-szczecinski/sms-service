# Introduction

SMS service based on Amazon SNS

# Service Description

This service allows you to send both Transactional (TAN, PIN etc.) and Promotional messages to end user.

The response contains informaitons related to the delivery status of the message as well as identifiers used to query the logs.

# Usage

The basic case requires the user to supply only the **message** and **receiverNumber**. You can provide an optional **senderId**
if you wish to have a custom "From" shown on the user's device.

## Creating an SMS

In order to create an SMS send a POST request to the endpoint. The message will be automatically split into multiple
parts if required (additional charges apply).

### Sample Request

```json
POST /sms-service/send
{
    "message": "Hello from VWFS",
    "receiverNumber": "+49123456789"
}
```

### Sample Response

```json
{
  "messageId": "uuid-uuid-uuid-uuid",
  "requestId": "uuid-uuid-uuid-uuid"
}
```

### Field explanation

| Field            |   Type   | Mandatory | Explanation                                                                                                                           |
| ---------------- | :------: | :-------: | ------------------------------------------------------------------------------------------------------------------------------------- |
| `message`        | `string` |    yes    | The body of the message to send. The emssage will be automatically split into 140 - 160 character parts if required.                  |
| `receiverNumber` | `string` |    yes    | Number to deliver the message to, following the E.164 standard                                                                        |
| `senderId`       | `string` |    no     | The **name** to be shown as the message origin. At maximum 11 alphanumeric characters can be used, one of which needs to be a letter. |
